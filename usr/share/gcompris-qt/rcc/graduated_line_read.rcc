qres     S%     Qu  ,/* GCompris - ActivityInfo.qml
 *
 * SPDX-FileCopyrightText: 2023 Bruno ANSELME <be.root@free.fr>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import GCompris 1.0

ActivityInfo {
  name: "graduated_line_read/GraduatedLineRead.qml"
  difficulty: 1
  icon: "graduated_line_read/graduated_line_read.svg"
  author: "Bruno ANSELME &lt;be.root@free.fr&gt;"
  //: Activity title
  title: qsTr("Read a graduated line")
  //: Help title
  description: qsTr("Read values on a graduated line.")
  //intro: "Find the value corresponding to the given spot on the graduated line."
  //: Help goal
  goal: qsTr("Learn to read a graduated line.")
  //: Help prerequisite
  prerequisite: qsTr("Reading and ordering numbers.")
  //: Help manual
  manual: qsTr("Use the number pad or your keyboard to enter the value corresponding to the given spot on the graduated line.") + ("<br>") +
          qsTr("<b>Keyboard controls:</b>") + "<ul>" +
          "<li>" +qsTr("Digits: enter digits") + "</li>" +
          "<li>" +qsTr("Backspace: delete the last digit") + "</li>" +
          "<li>" +qsTr("Delete: reset your answer") + "</li>" +
          "<li>" +qsTr("Space, Return or Enter: validate your answer") + "</li>" +
          ("</ul>")
  credit: ""
  section: "math numeration"
  createdInVersion: 40000
  levels: "1,2,3,4,5,6,7,8,9"
}
  ;<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" width="100" height="100" version="1.0" id="svg20"><metadata id="metadata2"><rdf:RDF><cc:Work><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/><dc:title/><dc:date>2024</dc:date><dc:creator><cc:Agent><dc:title>Timothée Giet</dc:title></cc:Agent></dc:creator><cc:license rdf:resource="http://creativecommons.org/licenses/by-sa/4.0/"/></cc:Work><cc:License rdf:about="http://creativecommons.org/licenses/by-sa/4.0/"><cc:permits rdf:resource="http://creativecommons.org/ns#Reproduction"/><cc:permits rdf:resource="http://creativecommons.org/ns#Distribution"/><cc:requires rdf:resource="http://creativecommons.org/ns#Notice"/><cc:requires rdf:resource="http://creativecommons.org/ns#Attribution"/><cc:permits rdf:resource="http://creativecommons.org/ns#DerivativeWorks"/><cc:requires rdf:resource="http://creativecommons.org/ns#ShareAlike"/></cc:License></rdf:RDF></metadata><rect style="opacity:.99956;fill:#e9e9e9;fill-opacity:1;stroke:none;stroke-width:2;stroke-linecap:round;stroke-linejoin:round" id="rect832" width="612.77094" height="173.85524" x="-275.58139" y="126.82097" rx="23.525999" ry="23.525999"/><rect style="opacity:.99956;fill:#fff;fill-opacity:1;stroke:none;stroke-width:2;stroke-linecap:round;stroke-linejoin:round" id="rect851" width="94" height="60" x="3" y="30" ry="5" rx="5"/><path style="opacity:.99956;fill:#e9e9e9;fill-opacity:1;stroke:#c27a33;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M10 70h80" id="path853"/><path style="opacity:.99956;fill:#e9e9e9;fill-opacity:1;stroke:#c27a33;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M90 60v20" id="path855"/><path style="opacity:.99956;fill:#e9e9e9;fill-opacity:1;stroke:#c27a33;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M10 60v20" id="path857"/><path style="opacity:.99956;fill:#e9e9e9;fill-opacity:1;stroke:#c27a33;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M50 60v20" id="path859"/><path style="opacity:.99956;fill:#e9e9e9;fill-opacity:1;stroke:#c27a33;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M30 65v10" id="path861"/><path style="opacity:.99956;fill:#e9e9e9;fill-opacity:1;stroke:#c27a33;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="M70 65v10" id="path863"/><rect style="display:inline;fill:#fff;fill-opacity:1;stroke:gray;stroke-width:2;stroke-linejoin:bevel;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" width="25" height="25" x="37.5" y="37.5" ry="5" id="rect10" rx="5"/><g id="g1600" transform="translate(18.565 24.17) scale(2.2476)" style="stroke-width:1.68157"><path style="fill:#f4a932;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:.422353px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="m11.297 9.032 2.13-1.349 2.323.25 1.016 1.448-.387 2.147-1.5 1.049-.388.949.34.949-1.017 1.048-1.55-.25v-1.048l.194-.4-.242-.849.436-2.297-.968.45z" id="path118"/><path style="font-style:normal;font-variant:normal;font-weight:400;font-stretch:normal;font-size:medium;line-height:100%;font-family:'Indigo Joker';text-align:start;writing-mode:lr-tb;text-anchor:start;fill:#9f6710;fill-opacity:1;stroke:none;stroke-width:.444915px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="M17.143 9.48c-.141 2.232-1.034 2.186-2.127 3.2.006.208-.053.412-.103.591 0 .012.037.074.111.185.45.7-.24 1.703-.897 1.957-.814.314-2.153.223-2.17-.784.014-.305.142-.559.247-.82a1.028 1.028 0 0 1-.187-.141c-.04-.053-.06-.215-.06-.485-.092-.807.318-2.011.367-2.264-.247.14-.51.292-.786.448-.287-.885-.389-1.649-.632-2.354a1.179 1.179 0 0 1-.077-.211c2.568-2.002 6.337-1.9 6.314.679zm-.786-.061c-.222-.976-1-1.464-2.332-1.464-.785.084-1.637.28-2.162.917-.074.088-.111.17-.111.247 0 .159.054.535.162 1.128.006.042.04.088.103.141.668-.493 2.232-.995 2.247.203-.193.576-.826.416-1.333.388-.283.57-.3 1.235-.3 1.948a.36.36 0 0 0 .035.141c.034.076.074.115.12.115.54-.162.994-.111 1.538-.326.023-.17.054-.418.094-.74 1.433-.24 1.981-1.373 1.94-2.698zm-1.982 4.187c-.587-.364-1.777.207-1.795.829.258.655 1.48.371 1.77-.062.217-.304.207-.516.025-.767z" id="path120"/></g></svg>  �  P*x���s۶����w�:[#�I�6����M?�[��m�n��)ms�EW��d]����DQ�D��m{L�� � � ������/�	K�y�����hx�b�����~퐯��8�߽g,���M�l���7٘���I��99������)��z	��ӄRo����?c�S��2�qƦ�&c���l����d/�g' <�a�%O2�:{�b���F��Z�ߑ7T�z�7�Oh���>	ƃY �i���?���G��L฀��ѿ�`��wdlͲ۝ut⧔|�!�X8&��˄/i�ݒ4KX<+n��!�^#ڏW�K���`@�y��ˈ���h:&�}"��8��`O��O�&��3�$�S���@F�S�z�;��/2?�My�J�$hq�/���G	��g�c�ؘ�\�y>n5�K?��%|��唯� �Z�A�F�A0�i:H���t��9�t=��8��$��,��Ы�S�ċ����9Фɚ�Kd��ѳ����xL�o	�f�{�A��@��X���1GX=ָ#:�����1��/�v� �R���c�tМ�0��'T���3�2�H�-_����3D`2h	��_�i��e9���O���C�.�P���B�^�-��Y\����vh?b0�J�Z������%|F�4��_�D�X]���fX91~�v�q$8����+���.EK4�?mp��s�&�k?!�?ո�X���!�}<�#?�\Lo��
?�NAt"��f>�<Z�B<$Fe��!�$��p�L�'d�K��:���4{���T-H�g��&�,����2b�@��Y�Q���h�]2]��H6�A�:YE��OhX�P�vRD����6�z�,Sq��dI�P�1�C��kg��F�&	Oހ��x��V�혗�G�r�e<NO"\�A��QJm&�"�K��W~2�6&�!��D_�S�%xXbN�C៥���u��ww�:��q:_�w������C�P�ϗ~ a�ˠ�LX���yڀ+��P�z_<¿����z�� ����|L^���[���߈w?�����hp ��5�V��?��9M��WNмa��2�a0��|��=�`�j��"�����_�x	��,�~�I�\����;� HX�0	�<q��2dlk�{���$%|�Wq
�rP*M��\#0�qb�T��{f�l�߻��D�W����A�����L�4
��$��6#����|�����*�ht˻�`�Vxy���]�J�G�A�������B/X!�#h�Hu��m�h�<ci&|�Odd��\�������fΗ�i��!`C4�:C��*��Pj��9�5z�f�0����f|wP8Ix\$�����#��\�J!6�XNh@��}X��b�2���&�-��ZtMB5��r����}�{j���t
�um%WA-���a#�6�g�u�=���HFI�����^C�-������3��
���&��S��<"�@O[FX���%ŬS��-Z�w��=:jdls;f�D�"�3�?���D�=gaHa�}��|��I>p}A��FP���l������i+�}*���٪+�E|-GN������ᙙ���f"����ey�i9�^'A�\�%�Xf�|Xy]W�aM�\��.D�VMܡ�j��6�0�\�����;��ֻM����L%���ȷ�r���5������h���2>r[�ښ��43�DRq�X�t��~��Ã����/�Xem>�vm\ߚ}~�aƱ}&��n�ؚC�#W�z��zm��7�VK}��%���Pk�1YP�<vF�X؛�5Se�f]�ߩ_��S�����m����nᶌC�&�G�����K�4��:lG���i��Z��q:��bL.`CH��	Xwy7c�V��ߒqcqS�"�1v!
k�F6�֛�����`}�\��B\VS���~嫅�����5�.v��242�0tg;^���-�[%�m���]�^v�&�K�7���2�t
���)��&Eqc#�������\l�R�j*��Z�S�'�RiI��ӟ*}Ҭ��e�۾���=1b�E��y7���\��À^�'qUl*N�$���<seK\��.����!�\g��V��C�L)_�B]���h����S�Zϭ���<�u�1Z�<	�
����'�x7n��4]�QTO���YRlK2������$�����m��M*��
qZٛ`an�慜��9��D1w)���v�r�Z��pj��o.(����Z��.��Y���Un[�u�P�ZRB���o-O�-���n����Ì�T�f�U�Q���Q��Ф�Gi�V����^!O���o�}�V�����-D8�(N��"�f�G�o	���b�ӿ���G����=6�;��hGNhL�Nh�&ڡ�C����D9��h�Nh&ځ�wߙx'��ҥP'���>tB��hO)n_m��jY�Q�0�l�4��(��ͭ�[�V����ّ�R�jug����˜��`F�����fP�,�9ME*7��T��JL���WI�V��[s��b-��YC���Gn;$���谾�R�`/'�;�p��w��r�Eq��^��_�YВ�DQX�<ak��^a"���4���a��Q���YQpc;�5��d��(������R���QKaH����	�"���=����UۯK���<�s�x=!rv9���q��\s&4�IM��ɪZ2|�'�߼�WI��)Wٽ�����77/�l�)[Gn��Qؚ����vd���U��B���͵�Mkc�P�ں��A]ɹn%+a�bȍ웩~����q����^V�\�'��U��u�	��c�n�U����Z5��|k�EM�*]�_�[�&�j�s}��W!F��6=�(��`�ɤLs�\"<�db��Qӂ����rd����>�	ܗ�Ʋ��Nӫ�0
�|{H�6������x�������sX.MX>��s)o��~�΅{�LIZ#�B��7�T��l���$
S�!�����!�٧������ZasF�w�ycX���+z�qض���[�>iK҆��6ڐ$�;��r��غ�l�\m��9G�#l[D�4/^xa���I�W��^��3)������] D�6��_��}hʫ˘&uء�|{�5�۝#�Z����_K��S�=�������n7��f_���)&v&�����c�we�~�"��8�Ȳ��G8��D~-a\y3�[BT��\���H3�I�]��{�-|Um�2�:]O4BL���;h܍���Q�&��[�̯`�~�n��)�avyT>��d�yJ�,��P��gòk�f���
²��j���M���o~����3A���u�_��X����2zT��E�E����f�p�|,�����m����$H��|A�����R.uZ�P���!>�U/h�lT'�i�1,��Z	��7@�*�eް�q�1�c\��T
h�m�w�s�z���ZQ6yuY���H�чR�!�T�K��77R��UC���ii��1��a�����)�q"��H����o
��A~�ʼ����"NY�J�o�T�]�:�[ѫ�&���]b��pu����?�cu����S�v�y��e��"K~e�#���Wu�	��U���M
��d��-��r�S�ӥ�jNoa�߻7�u��k�����mK�`N��74]E�)/�B�l�=Gj?¯�mM�y��b|�'�o��^�է��ӕ?e32��G5T�,.>V�<�	��GO�\��kk��5<$�S�����q�)��  	�  $�x��kS�:�{~���N톘����\n��������a[N�8V�$S����=G�l���n�vH|tt�/)��ɻ}�ZK�Ș,$M
�Yr��]HF��j@��2;��?�y����F��R��k�Kv&;/�[Y䂼�0;8z@��,�B����(����G<f�b�Ä嚧��]���h�"���g�\Z~ۃh-�bEI��ʛA�Wk!5��?<�$;�t�PE�[�Z�3�&�Z�oo�TH�f��xL5�a��j��W�n/b���d���0�i������J��9��#v�2ᚭ��Fcͯ��y/f +z=Y�,g�7F^�I	_��c� ��-�H����d7sAeB�%�1PRDd���y®-��|fC筗<V$�bEP��	��iM�Q��

x�9S
8� &c�B�����c��H�'��@��|!��B� �^Fi&��W�� 7�[�B&�R��R(��YH���#�������J��Y�C�f$ڸ+�^ 2~0��*����y	�J�<yփ��Y�E#�DzJ��
�R���
3$#\�Ƞ$�)�t�H��/OI`ȽB�B'��JY���&�!�4;(}��z��"cj�(������-�k�5 r�/�L�W����i��6!�ĳ�)�G|ŵ�-�xJ����;��C����L���ZSk�2�z�N���ƥ��+c�g�c��	+Ҿ�A������S�J�uτ��X0UB5%PJ "��<�����K�bY�.�>�{<n�|i,���-���4lи}��!�Ja�AvT�v°߫v��]�^ǁ�T��nx��,�Ӥ�5Te=�"�-o� ~xP#��g1�����H�9�o�����-��y�l7�M��Ђ��7P(��JIo0r\64ZE�Y/C2'���D���K\H	�g��GF6�75%
����>�<R�X �|���8�+f>?���Z����_j�B�Ԧ���'!�r���
/M��T�Z r�`ny��f6�Uf�օZ@��\M���%HJ^;��ȶ��M��^����bn<`[��9�����o	}%qlȢ8c�U��pmE����u�i4P�l�J�T�h�bF�e��4���$�h+�qԲHӌnH6=��r�G|����d��*z��*̳�*vީ@)2߫�s�̶@/H�e�נѬ���
��O���t�N��y���"�i�2�k7R�pZ�񬮭f�@����W7b�Q���5��F�ￓ���4�~�/�$܎b�^�<	��!�˂]�R� ��l��v�q�e[�d�1rQ��^����;Ң��2�����U&���3#�1�'�T��<��dBv{2V�vլ�����F���/l3;�p�n��X0�<#k�Я4��+��P_Q�\ˌ/J��Es��ڽ���+�aU���+;T۱kGC��2ۖB(�Xfe���"������@�\(g�nC���7�f��N�U|��|�1�T@E�.�3�E��=��A����T��uA,���Hs8�����=f��!��_թ���9���9�s/�N��ՠ�!Ӱ�J\1̖��uGT�|�տy����8�7�m�li^h6{����%�Rv�lS��5j��2�H�8HSk�!�Y�l���?QȘm+T^m��qQ}�W��;b�<*�va
���������CL4���K_�0Ud�W#� bvȂ���"��%KJ�d��YȜ�1��5�"�cK�p�nx�zTBF1�^�곮}�Mm��RS*�j��(ϡ�]z���z�ǔ��03�e�=�n�K����L��A�钰Ĉ�1�4U&������h�ư��l�`JJ�����,�ZZY[4���I)�	��9�:vܮЍL586n.���U��s�%Ҝ ����l\$طy��\G"�p�q�Ñw1<c��'��5��P ��T |`�B}̛���3fćb����d�p�5����/Ea�M�!D̰~!-���F�M"������C �Q������-7����>���]�#R�g`H�BhCHz�Ġ�jX���湗]���kb�7>��� 䫕C�S��2Z������CE��������&G�%��P�����,$Zй����?^;�'�Ny���I0TE��qo�+��`nZ|m����O/2�����[����/�@��_؍
.���;{��^��۴��<���8��.���C>�lk2.�zBh�2�w�2]`3)w��Ap��lMc���ku��f�����O�����WLYO�Z�ۜ�^��w�6M��n}��@�i}���M���S��a4�zu��tA/��?uA������[�����Kw��a�	��}��~DH�^���g�g���z�����Lb�w��/�����π���� ���av�^)��N�o=c�U�,X�|}oν��+� �p����Q�.4�"�i{4�'>�������r8�Q��1�������do�0�&���js$  "<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 60 60" version="1.1" id="svg8"><metadata id="metadata2"><rdf:RDF><cc:Work><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/><dc:title/><dc:date>2024</dc:date><dc:creator><cc:Agent><dc:title>Timothée Giet</dc:title></cc:Agent></dc:creator><cc:license rdf:resource="http://creativecommons.org/licenses/by-sa/4.0/"/></cc:Work><cc:License rdf:about="http://creativecommons.org/licenses/by-sa/4.0/"><cc:permits rdf:resource="http://creativecommons.org/ns#Reproduction"/><cc:permits rdf:resource="http://creativecommons.org/ns#Distribution"/><cc:requires rdf:resource="http://creativecommons.org/ns#Notice"/><cc:requires rdf:resource="http://creativecommons.org/ns#Attribution"/><cc:permits rdf:resource="http://creativecommons.org/ns#DerivativeWorks"/><cc:requires rdf:resource="http://creativecommons.org/ns#ShareAlike"/></cc:License></rdf:RDF></metadata><path style="fill:#633e0c;fill-opacity:1;stroke:none;stroke-width:.9375px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="M262.464 469.244h60l-30 48.75z" transform="translate(-262.464 -457.994)" id="path4"/><path style="fill:#e99e33;fill-opacity:1;stroke:none;stroke-width:.9375px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="m292.464 510.494-22.5-37.5h45z" transform="translate(-262.464 -457.994)" id="path6"/></svg>  /* GCompris - graduated_line - Data.qml
 *
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@free.fr>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import GCompris 1.0

Data {
    objective: qsTr("Number to find between 1 and 7.")
    difficulty: 2
    data: [
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": true,
                "range": [1, 7],
                "steps": [1],
                "segments": []
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": false,
                "range": [1,7],
                "steps": [1],
                "segments": [4, 6]
            }
        }
    ]
}
  </* GCompris - graduated_line - Data.qml
 *
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@free.fr>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Authors:
 *   Bruno Anselme <be.root@free.fr>
 *
 * When fitLimits is true:
 *  - the number of steps must be a divisor of the upper limit
 *  - segments is not used (empty)
 * range[min, max] (pair) ruler's min and max values
 * steps[a, b, c, ...] (list) of possible step values
 * segments[min, max] (pair) number of segments (random within interval). Unused when fitLimits is true
 *
 * Number of graduations = number of segments + 1
 * Constraint : segments[max] * steps[max] <= range[max] (Minimal check made with js: console warning and values adjusted)
 */
import GCompris 1.0

Data {
    objective: qsTr("Number to find between 1 and 5.")
    difficulty: 1
    data: [
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 6,
                "fitLimits": true,
                "range": [1,5],
                "steps": [1],
                "segments": []
            }
        }
    ]
}
  @/* GCompris - graduated_line - Data.qml
 *
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@free.fr>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import GCompris 1.0

Data {
    objective: qsTr("Number to find between 0 and 1 000 000.")
    difficulty: 5
    data: [
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": true,
                "range": [0, 1000000],
                "steps": [50000, 100000],
                "segments": []
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": false,
                "range": [0, 1000000],
                "steps": [10000, 100000],
                "segments": [5, 10]
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 15,
                "fitLimits": false,
                "range": [0, 1000000],
                "steps": [10000, 100000],
                "segments": [4, 12]
            }
        }
    ]
}
  n  9x�ݒ�J�@���C��$q��M�"�P�$�l�춛�J��M�J�BAPt؅��og������)��*��i^����"�.p�r�J�A���<�OHᑙ�X*���9�����������P��F�ޗ1�vo�HtPx��f��6���a؋Dhl�\���ʩ��1_	�k&���L���GL`V��-��.3��$�2�'DR��������IJ�Ԋ_�E\�F�����>+���V�2ߘok����W�mJg�Ɗ���E���C*��m�k��a$W��xT1N���x��RX�N���Ko���S����Bě� �~C�����B���R��5��+��66  &/* GCompris - graduated_line - Data.qml
 *
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@free.fr>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import GCompris 1.0

Data {
    objective: qsTr("Number to find between 0 and 1 000.")
    difficulty: 4
    data: [
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": true,
                "range": [0, 1000],
                "steps": [50, 100],
                "segments": []
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": false,
                "range": [0, 1000],
                "steps": [50, 100],
                "segments": [5, 10]
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 15,
                "fitLimits": false,
                "range": [0, 1000],
                "steps": [10, 100, 200],
                "segments": [4, 12]
            }
        }
    ]
}
  h  x�ՔoK�0���S}��v�� ED�P��}!�!�z�'iҥW�0��M*Lĉ�T<H��%������PW��B(MV�cq'I�M�2΢E%=ر\OF��)I���Py�7��)��� NL�4�e�p�cd��#a#a���4G��*&AhR8���A�ڄ��o,�󨪵����(�<W,=����3=b
���l�m��� H�#?!*�!��$�#��X�4o%��������0��.�v�L,�O��	ֺߴ�/?�;I��ņI+�$q��c�������T�J�Ɓ30ۀ4��;a�l���l�;b�A^�W��_�s��.2����������_�ؿ�w-���e���[y���.  k  �x��QMK�@��W9iI���D��P���R$if��f��L�R��ݤ�R�� =��.��{3;o�^�~Wg3C9�05qZČ�$�6Ћ9�t�f<����k��ճ����#h��\�Bi�T9��,��h�� o�4AK�oRTL��Dп�� �����Xrݡl�o�k����k:y�	�F0χ�Ľ-��A*��QA�}u��=��R�&��E�u��`T��-�u�X�m��6xi�)$�_�+H%w���I����r�2�gS���մ�`z���F�8+��a�fv�c��6��w4ٍ��;�T��s�"���7��!����M���^��|���������a�  /* GCompris - graduated_line - Data.qml
 *
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@free.fr>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import GCompris 1.0

Data {
    objective: qsTr("Number to find between 0 and 20.")
    difficulty: 3
    data: [
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 8,
                "fitLimits": true,
                "range": [0, 20],
                "steps": [1],
                "segments": []
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 8,
                "fitLimits": true,
                "range": [0, 20],
                "steps": [2],
                "segments": []
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": false,
                "range": [0, 20],
                "steps": [1, 2, 4],
                "segments": [5, 10]
            }
        }
    ]
}
  /* GCompris - graduated_line - Data.qml
 *
 * SPDX-FileCopyrightText: 2023 Bruno Anselme <be.root@free.fr>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
import GCompris 1.0

Data {
    objective: qsTr("Number to find between 1 and 10.")
    difficulty: 2
    data: [
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": true,
                "range": [1, 10],
                "steps": [1],
                "segments": []
            }
        },
        {   "title": objective,
            "rules": {
                "nbOfQuestions": 10,
                "fitLimits": false,
                "range": [1, 10],
                "steps": [1],
                "segments": [5, 9]
            }
        }
    ]
}
 
dt� g c o m p r i s   z� s r c 
 �Z� a c t i v i t i e s �#� g r a d u a t e d _ l i n e _ r e a d �Ǖ r e s o u r c e b�| A c t i v i t y I n f o . q m l �҇ g r a d u a t e d _ l i n e _ r e a d . s v g ß� G r a d u a t e d L i n e R e a d . q m l :� g r a d u a t e d _ l i n e _ r e a d . j s    3 3    4 4    5 5 		j�� a r r o w . s v g    6 6    7 7    8 8    9 9    1 1    2 2 �a| D a t a . q m l                                                           "                  <                       &Z  ��B   �       0  ��B   ~           ��B   �      o  ��B   h    
   
          �                 �                 :                 B                 J                 j                 r                 z                 �                 R       0$  ��B  �       =�  ��B  �      A�  ��B  �       CH  ��B  �      Gr  ��B  �      H�  ��B  �       JM  ��B  �       NU  ��B  �       6J  ��B  �       9R  ��B